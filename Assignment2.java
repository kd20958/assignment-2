/* 02-12-14
   Kevin Do
   Enter five numbers to find the sum, average, max, and min.
*/
import java.util.Scanner;

public class assignment2 {

	public static void main (String[] args) {
		
		int count = 0;
		double [] array = new double [5];

		Scanner keyboard = new Scanner (System.in);
		
		System.out.print("Hi, please enter the first number ");
			double num1 = keyboard.nextDouble();
			System.out.println(num1);

			array[count] = num1;
			count++;

		System.out.print("enter the second number ");
			double num2 = keyboard.nextDouble();
			System.out.println(num2);

			array[count] = num2;
			count++;

		System.out.print("enter the third number ");
			double num3 = keyboard.nextDouble();
			System.out.println(num3);

			array[count] = num3;
			count++;

		System.out.print("enter the fourth number ");
			double num4 = keyboard.nextDouble();
			System.out.println(num4);

			array[count] = num4;
			count++;

		System.out.print("enter the fifth number ");
			double num5 = keyboard.nextDouble();
			System.out.println(num5);

			array[count] = num5;
			count++;

		double sum1;
		sum1 = num1 + num2 + num3 + num4 + num5;

		double average1;
		average1 = sum1 / 5;
		
		double max1 = array[0];
		double min1 = array[0];
		for (int n = 1; n < 5; n++){
			if (array[n] > max1){
				max1 = array[n];
			}
			if (array[n] < min1){
				min1 = array[n];
			}
		}

		System.out.println("the sum is " + sum1);
		System.out.println("the average is " + average1);
		System.out.println("the max is " + max1);
		System.out.println("the min is " + min1);
		
	}
}